# Sequential Number Typer

This script was developed using [AutoHotKey](https://www.autohotkey.com/#keyfeatures).

When run, the script will prompt the user for a range of numbers, and then automatically enter the numbers within that range with each number entry followed by an enter key press. It will automatically stop running if the current active window is changed. This script was built for entering data in a SharePoint list.
